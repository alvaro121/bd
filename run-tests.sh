#!/usr/bin/env bash

# make sure we're in the project directory
cd $(dirname "$0")

# just to include the functions defined in ./bd
_BD_TEST=true . ./bd /dev/null > /dev/null

red='\e[0;31m'
green='\e[0;32m'
nocolor='\e[0m'

success=0
failure=0
total=0

assertEquals() {
    ((total++))
    expected=$1
    actual=$2
    if [[ $expected = $actual ]]; then
        ((success++))
    else
        ((failure++))
        {
            echo Assertion failed on line $(caller 0)
            echo "  Expected: $expected"
            echo "  Actual  : $actual"
            echo
        } >&2
    fi
}

doTest() {
    local oldpwd="$1"
    shift
    local expected="$1"
    shift

    local res="$(_bd_gen_newpwd "$oldpwd" "$@")"

    assertEquals "$expected" "$res"
}

# should do nothing if run with no args
doTest "/usr/share/info" "/usr/share/info"

# should jump for exact match
doTest "/usr/share/info" "/usr/share/" \
    "share"

# should jump for closest exact match
doTest "/usr/share/info/share/bin" "/usr/share/info/share/" \
    "share"

# should do nothing for prefix match without -s
doTest "/usr/share/info" "/usr/share/info" \
    "sh"

# should jump for prefix match with -s
doTest "/usr/share/info" "/usr/share/" \
    -s "sh"

# should jump for closest prefix match with -s
doTest "/usr/share/info/share/bin" "/usr/share/info/share/" \
    -s "sh"

# should do nothing for mismatched case prefix match without -si
doTest "/usr/share/info" "/usr/share/info" \
    -s "Sh"

# should jump for mismatched case prefix match with -si
doTest "/usr/share/info" "/usr/share/" \
    -si Sh

# should jump for mismatched case prefix match with -si
doTest "/usr/sHAre/info" "/usr/sHAre/" \
    -si Sh

# should jump for closest mismatched case prefix match with -si
doTest "/usr/share/info/share/bin" "/usr/share/info/share/" \
    -si "Sh"

# handling spaces: should do nothing for prefix match without -s
doTest "/home/user/my project/src" "/home/user/my project/src" \
    "my"

# handling spaces: should jump for exact match
doTest "/home/user/my project/src" "/home/user/my project/" \
    -s my

if [[ $failure = 0 ]]; then
   test_color="$green"
else
   test_color="$red"
fi

cat <<EOF

${test_color@E}Tests run: $total ($success success, $failure failed) ${nocolor@E}

EOF
